﻿We are focussed on government policy and public investment in the physical infrastructure of cities and communities (SDG 11) and these considerations:

* policy should maximise social, environmental and economic benefits equitably to current and future generations (we use the SDG’s as a way of categorising these benefits);
policy will realise this aspiration more effectively if it is developed and administered with the benefit of the latest scientific understandings and most informed foresight analyses;
* the exponential growth of scientific discovery (in, for instance, ecological and geological sciences, economics and sociology) and the sheer complexity of interactions between policy drivers, outcomes and environments are outstripping the capacity of individuals to interpret and develop informed responses to the rapidly evolving information and policy environments; and
* two areas of academic research offer the basis for innovation to address these challenges:
  - machine learning and automatic text analysis, which enables the screening and interpretation of vast quantities of science literature and
  - complexity modelling and scenario mapping, which are increasingly powerful tools for assessing sustainability and understanding impact pathways.
 
Advances in data science, joined with digital visualisation, communication and interaction tools (‘data storytelling’) can help policy professionals access and understand extensive and complex developments of scientific discovery, communicate ideas, and develop informed strategies. The inverse is also true: the potential of more powerful tools to propogate misinterpretation, misunderstanding and miscommunication is significant. This research examines how the advantages of data science and computing can be leveraged to realise more powerful analyses for policy development, without generating unacceptable risks of unintended consequences.

 